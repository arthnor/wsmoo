/*
Copyright 2019 Andrew Walbran

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

extern crate tokio;
extern crate websocket;

use std::env;
use std::fmt::Debug;
use std::net::SocketAddr;
use std::net::ToSocketAddrs;

use websocket::client::r#async::Client;
use websocket::message::OwnedMessage;
use websocket::r#async::{Server, TcpStream};
use websocket::result::WebSocketError;
use websocket::server::InvalidConnection;

use futures::compat::{Future01CompatExt as _, Sink01CompatExt as _, Stream01CompatExt as _};
use futures::future::{Future, FutureExt, TryFutureExt};
use futures::sink::SinkExt;
use futures::stream::{select, StreamExt, TryStreamExt};
use legacy_futures::Stream as _;
use tokio::codec::{Framed, LinesCodec};
use tokio::executor::spawn;

struct Session {
    server_address: SocketAddr,
    client: Client<TcpStream>,
}

#[derive(Debug)]
enum MessageFrom<L, R> {
    Server(L),
    Websocket(R),
}

impl Session {
    pub fn new(server_address: &SocketAddr, client: Client<TcpStream>) -> Self {
        Session {
            server_address: *server_address,
            client: client,
        }
    }

    pub async fn run(self) -> Result<(), WebSocketError> {
        let (ws_sink, ws_stream) = self.client.split();
        let mut ws_sink = ws_sink.sink_compat();
        let server = TcpStream::connect(&self.server_address);

        // Think of this like a try/finally block. We always want to send a Close
        // message to the websocket if anything goes wrong.
        let ret = async {
            let s = server.compat().map_err(|e| dbg!(e)).await?;
            let (server_sink, server_stream) = Framed::new(s, LinesCodec::new()).split();
            let mut server_sink = server_sink.sink_compat();

            let mut combined = select(
                server_stream.compat().map(MessageFrom::Server),
                ws_stream.compat().map(MessageFrom::Websocket),
            );

            while let Some(x) = combined.next().await {
                match x {
                    MessageFrom::Server(Ok(mut line)) => {
                        line.push('\n');
                        ws_sink.send(OwnedMessage::Text(line)).await?;
                    }
                    MessageFrom::Websocket(Ok(m)) => {
                        match m {
                            OwnedMessage::Close(_) => break,
                            OwnedMessage::Ping(p) => ws_sink.send(OwnedMessage::Pong(p)).await,
                            OwnedMessage::Pong(_) => continue,
                            OwnedMessage::Text(t) => {
                                server_sink.send(t).await?;
                                continue;
                            }
                            _ => ws_sink.send(m).await,
                        }?;
                    }
                    MessageFrom::Server(Err(_)) | MessageFrom::Websocket(Err(_)) => {
                        dbg!(x);
                        break;
                    }
                }
            }
            Ok::<(), WebSocketError>(())
        }
            .await;

        println!("sending final close");
        ws_sink.send(OwnedMessage::Close(None)).await?;

        ret
    }
}

async fn run(listen_address: impl ToSocketAddrs, server_address: impl ToSocketAddrs) {
    let server_address = server_address.to_socket_addrs().unwrap().next().unwrap();

    let server = Server::bind(listen_address, &tokio::reactor::Handle::default()).unwrap();

    let mut stream = server.incoming().compat();
    loop {
        let conn = match stream.try_next().await {
            Err(InvalidConnection { ref error, .. }) => {
                println!("Connection error: {}", error);
                continue;
            }
            Ok(None) => continue,
            Ok(Some(v)) => v,
        };
        let (upgrade, addr) = conn;
        println!("New client {}", addr);

        let f = async move {
            let (s, _) = upgrade.accept().compat().await?;
            let session = Session::new(&server_address, s);
            session.run().await
        };

        spawn_future(f, "Client Status");
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 3 {
        println!("Usage:");
        println!(
            "  $ {} <listen address>:<port> <backend server address>:<port>",
            &args[0]
        );
        return;
    }
    let mut runtime = tokio::runtime::Builder::new().build().unwrap();
    runtime
        .block_on(
            run(args[1].clone(), args[2].clone())
                .boxed()
                .unit_error()
                .compat(),
        )
        .unwrap();
}

fn spawn_future<F, I, E>(f: F, desc: &'static str)
where
    F: Future<Output = Result<I, E>> + 'static + Send,
    E: Debug,
{
    spawn(
        async move {
            match f.await {
                Err(e) => println!("{}: '{:?}'", desc, e),
                Ok(_) => println!("{}: Finished.", desc),
            }
        }
            .unit_error()
            .boxed()
            .compat(),
    );
}
